enum Emptype:String{
    case Permanent = "Permanent"
    case Temporary = "Temporary"
}





class Employee{          //employee class definition
    
    var id : Int = 0
    var name :String
    var phno :String
    var domain:String
    var type :Emptype
    
    //initialisers for attributes
    init (id :Int,name:String,phno:String,empType:String,domain:String){
        self.id=id
        self.name=name
        self.phno=phno
        self.type = Emptype(rawValue:empType)!
        self.domain=domain
        
        
    }
    
    func describe() -> String         // returns string containing  description of an employee
    {
        return "\(self.id)   \(self.name)  \(self.phno)  \(self.domain)"
    }
    
}

//class Greytip definition
class GreyTip {
    
    var employees = [Employee]()
    
    
    func addEmployee(employee:Employee)    //add the employee
    {
        employees.append(employee)
        
    }
    
    
    
    func deleteEmployeeById(delempid :Int) -> Bool     //delete employee based on id
    {
        var i:Int=0
        
        for employee in employees{
            if employee.id==delempid {
                print("deleted employee")
                employees.removeAtIndex(i)
                
                print("\(employee.id) \(employee.name) \(employee.phno) \(employee.type)"  )
                return false
            }
            else { i=i+1 }
        }
        
        return true
    }
    
    
    func printAllEmployees()        //print  all employees in record
    {
        var  employeelist : String="\0"
        
        print("Employee list")
        for emp in employees{
            
            employeelist=employeelist+emp.describe()+"\n"
            
        }
        print("\(employeelist)")
    }
    
    
    func printEmployeeType(type : String){       //print employees based on type
        
        print("Employees of type  " + "\(type)")
        for empl in employees {
            if String (empl.type)==type{
                
                print(" \(empl.id) \(empl.name) \(empl.phno) \(empl.type)")
            }
        }
        
    }
    
    
    func serachEmployeeById(key : Int) -> Employee?    //search employee
    {
        
        print("searched employee")
        for employ in employees
        {
            if employ.id==key{
                
                return employ
            }
        }
        return nil
        
        
    }
    
    func editEmployeeById(key:Int,type: String,value:String)     // edit employee details
    {
        var result:BooleanType=true
        for empl in employees {
            if (empl.id)==key{
                result=false
                if(type=="name")  {
                    empl.name=value
                }else if type=="domain"{
                    empl.domain=value
                }else if  type == "emplType"{
                    empl.type=Emptype(rawValue:value)!
                }  else if type=="phno"{
                    empl.phno=value
                }
                
                
            }
        }
        if result {
            print ("not found")
        }
        
    }
}


let  greyTip = GreyTip()
var result : Bool=false


var employeeA = Employee(id:1, name:"radhika", phno:"987776456",empType:"Permanent",domain:"ios")
greyTip.addEmployee(employeeA)       // add employee


var  employeeB = Employee(id:2,name:"divya",phno:"98745786",empType:"Temporary",domain:"ios")
greyTip.addEmployee(employeeB)       // add employee


var employeeC = Employee(id:3,name:"anusha",phno:"97568887456",empType:"Permanent",domain:"ios")
greyTip.addEmployee(employeeC)       // add employee

var employeeD = Employee(id:4,name:"aishwarya",phno:"988987456",empType:"Permanent",domain:"android")
greyTip.addEmployee(employeeD)      // add employee

greyTip.printAllEmployees()      // print employee list

result = greyTip.deleteEmployeeById(6)      // delete employee by Id
if result{
    print ("delete id not found")
}



greyTip.editEmployeeById( 3,type: "name",value: "radha")      // edit employee details


greyTip.editEmployeeById( 1,type: "phno",value: "900000000")

greyTip.editEmployeeById( 2,type: "domain",value: "android")

greyTip.editEmployeeById( 4,type: "emplType",value: "Temporary")

greyTip.printAllEmployees()        // print employee

var e = greyTip.serachEmployeeById(1)        // search employee by Id
if(e==nil){
    print ("not found")
}
else
{
    print("\(e!.id)", "" ,"\(e!.name)","" ,"\(e!.phno)","" ,"\(e!.type)")
}

greyTip.printEmployeeType("Permanent")     // print employee list based on type
