//
//  ViewController.swift
//  Calculator
//
//  Created by Radhika on 8/2/16.
//  Copyright © 2016 Radhika. All rights reserved.
//

import UIKit



class ViewController: UIViewController {

   
    @IBOutlet weak var display: UILabel!
    var op1=0
    var op2=0
    var result : [String] = []
    var result2 : [String] = ["+", "-","*","/"]
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBAction func didTapClear(sender: AnyObject) {
        display.text = ""
        result.removeAll()
        op1=0
        op2=0
        

        
    }
    
    @IBAction func didTapOperand(sender: AnyObject) {
        
         let  buttonvalue = (sender as? UIButton)!.tag
          let buttonvalue1  = String(buttonvalue)
          var checkvalue = false
         for checkop in result2 {
            if result.contains(checkop){
                 checkvalue = true
            }
        }
        
        
        if checkvalue{
           op2 = buttonvalue
            result[2] = buttonvalue1
        }else{
            op1=buttonvalue
            result[0] = buttonvalue1
        }
   
    }
    
    @IBAction func didTapOperator(sender: AnyObject) {
        
        let  value2 = (sender as? UIButton)!.tag
        let   value3 =  String (value2)
        
       result[1]=value3
        
       }
    
    
        
    
    
    @IBAction func didTapResultButton(sender: AnyObject) {
        
        var resultvalue = 0
        
        var operation = result[1]
        
        switch operation{
            
        case "+" : resultvalue = op1+op2
        case "-" : resultvalue = op1-op2
        
        default  : resultvalue = 0
            
        }
        
        
        display.text = String(resultvalue)
}
}