//
//  SecondViewController.swift
//  Push
//
//  Created by Radhika on 8/2/16.
//  Copyright © 2016 Radhika. All rights reserved.
//

import UIKit

class SecondViewController: UIViewController {

    
@IBOutlet weak var field1: UILabel!
    var text : String? = nil
   
    @IBOutlet weak var clearButton: UIButton!
   override func viewDidLoad() {
    super.viewDidLoad()
    // Do any additional setup after loading the view, typically from a nib.
    
    field1.text=text
    field1.hidden = false
}

  override func didReceiveMemoryWarning() {
    super.didReceiveMemoryWarning()
    // Dispose of any resources that can be recreated.
}
//
@IBAction func btnSubmit(sender: UIButton) {
//    
    field1.hidden = true
//    
}

    
    
    
    @IBAction func gotoBack(sender: UIButton) {
        
        
  //  navigationController?.popViewControllerAnimated(true)
        self.dismissViewControllerAnimated(true, completion: nil)
        
    
    }
    

}